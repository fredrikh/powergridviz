from cgi import test
from utils import Grid, base_station_distances, cartesian_sum, euclidian_distance, polar_to_cart, euclidian_distance
from pytest import approx

def test_grid_creation():
    power_grid = Grid('grids/test_grid.yaml')
    assert len(power_grid.graph.edges) == 9
    assert power_grid.graph.edges['2', '3']['length'] == 2
    assert power_grid.graph.edges['3', '4']['id'] == 'branch_3'
    assert power_grid.graph.nodes['1']['Pd'] == 3
    assert power_grid.graph.nodes['2']['Qd'] == 0

def test_cartesian_sum():
    # List of test problems. Each element is a list of three tuples where
    # the sum of the first two should equal the third
    test_problems = [
        [(0, 0), (0, 0), (0, 0)],
        [(0, 1), (1, 0), (1, 1)],
        [(2, 4), (3, 8), (5, 12)],
        [(-4, 6), (-1, -7), (-5, -1)]
    ]
    for problem in test_problems:
        assert cartesian_sum(problem[0], problem[1]) == problem[2]

def test_polar_to_cart():
    # List of test problems. Each element is a list on the form
    #   [r, phi, (x, y)]
    # where the polar coordinate given by r and phi should equal (x, y)
    test_problems = [
        [0, 0, (0, 0)],
        [1, 0, (1, 0)],
        [1, 90, (0, 1)],
        [1, 45, (0.70710678118655, 0.70710678118655)]
    ]
    for problem in test_problems:
        assert polar_to_cart(problem[0], problem[1]) == approx(problem[2])

def test_changing_bus_positions():
    power_grid = Grid('grids/test_grid.yaml')
   
    # Change the angle of the branch between bus 2 and 6 to straight north
    power_grid.branch_angles[('2', '6')] = 90

    # Check that buses 6 and 7 have been changed appropriately
    positions = power_grid.get_bus_coordinates()
    assert positions['6'] == approx((1, 1.5))
    assert positions['7'] == approx((6, 1.5))

    # Move the rest of the branches like in the figure in grids/test_grids.txt
    power_grid.branch_angles[('6', '7')] = 90
    power_grid.branch_angles[('4', '8')] = 270
    power_grid.branch_angles[('8', '9')] = 270

    # Make the branch to bus 10 at a strange angle
    power_grid.branch_angles[('8', '10')] = 317

    # Check that all buses have been placed correctly
    positions = power_grid.get_bus_coordinates()
    assert positions['1'] == approx((0, 0))
    assert positions['2'] == approx((1, 0))
    assert positions['3'] == approx((3, 0))
    assert positions['4'] == approx((3.5, 0))
    assert positions['5'] == approx((6.5, 0))
    assert positions['6'] == approx((1, 1.5))
    assert positions['7'] == approx((1, 6.5))
    assert positions['8'] == approx((3.5, -2))
    assert positions['9'] == approx((3.5, -3))
    assert positions['10'] == approx((5.6940611048575, -4.045995080187501))

def test_euclidian_distance():
    # Set of test problems, where each problem is a list of two points, and their distance
    test_problems = [
        [(0, 0), (0, 0), 0],
        [(1, 1), (1, 1), 0],
        [(0, 0), (0, 2), 2],
        [(0, 0), (2, 0), 2],
        [(2, 0), (0, 0), 2],
        [(0, 2), (0, -2), 4],
        [(1, 2), (-4, -2), 6.403124237432849]
    ]
    for problem in test_problems:
        assert euclidian_distance(problem[0], problem[1]) == approx(problem[2])

def test_base_station_distances():
    power_grid = Grid('grids/test_grid.yaml')
   
    power_grid.branch_angles[('2', '6')] = 90

    base_stations = {
        'bs1': (2, 1.5), # 1 away from bus 6
        'bs2': (1, 4.5)  # 3 away from bus 6
    }
    distances = base_station_distances(power_grid, base_stations)
    assert distances['6']['bs1'] == approx(1)
    assert distances['6']['bs2'] == approx(3)

def test_randomize_positions():
    power_grid = Grid('grids/test_grid.yaml')

    # Test that we get a new seed each time we call the randomize function with no arguments
    seed1 = power_grid.randomize_placement()
    placement1 = power_grid.get_bus_coordinates()
    seed2 = power_grid.randomize_placement()
    placement2 = power_grid.get_bus_coordinates()
    assert seed1 != seed2
    assert placement1 != placement2

    # Test that we get the same placement if we specify the same seed
    seed3 = power_grid.randomize_placement(123)
    placement3 = power_grid.get_bus_coordinates()
    seed4 = power_grid.randomize_placement(123)
    placement4 = power_grid.get_bus_coordinates()
    assert seed3 == seed4
    assert placement3 == placement4

    # Test that we get different placements if we specify different seeds
    seed5 = power_grid.randomize_placement(0)
    placement5 = power_grid.get_bus_coordinates()
    seed6 = power_grid.randomize_placement(1)
    placement6 = power_grid.get_bus_coordinates()
    assert seed5 != seed6
    assert placement5 != placement6