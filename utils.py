from email.mime import base
import yaml
import networkx as nx
import math
import matplotlib.pyplot as plt
import matplotlib as mpl
import random

def cartesian_sum(coord1, coord2):
    """Get the sum of two points in cartesian coordinates"""
    return (coord1[0] + coord2[0], coord1[1] + coord2[1])

def polar_to_cart(r, phi):
    """Convert from polar coordinates to cartesian"""
    x = r * math.cos(math.radians(phi))
    y = r * math.sin(math.radians(phi))
    return (x, y)

class Grid:
    """
    A class to represent a power grid.

    Attributes:
    graph: networkx.Graph
        A graph representation of the power grid. Each bus is a node and each
        branch is an edge. The length of the branches are stored in each edges
        'length' attribute.
    branch_angles: dict[str, float]
        A dict mapping branch ids to an angle in degrees. This is used to place each node
        geographically. The angle 0 means directly right/east, and all angles are
        initialized to 0.
    origin_bus: str
        The id of the bus that will be used as origin when the grid is drawn
    """

    def __init__(self, yaml_file):
        """
        Instatiates a grid from a given yaml file. The yaml file should contain an object\n
        with a dict attribute called branches that maps branch ids to objects with a 'from'\n
        and 'to' object referencing the buses at the start and end of the branch, respectively.\n
        Additionally, each branch should have a 'length' object with the length of the branch.\n
        It should also have an 'origin_bus' that gives the bus to be used as the root when\n
        drawing the graph.\n
        See the file grids/test_grid.yaml for an example.

        Arguments:
        yaml_file: str
            filename of a yaml file to load
        """
        with open(yaml_file) as inputfile:
            grid_data = yaml.safe_load(inputfile)
        self.graph = nx.Graph()
        self.origin_bus = grid_data['origin_bus']
        self.branch_angles = {}
        for bus, demand in grid_data['bus_demands'].items():
            self.graph.add_node(bus, Pd=demand['Pd'], Qd=demand['Qd'])
        for id, branch in grid_data['branches'].items():
            self.graph.add_edge(branch['from'], branch['to'], length=branch['length'], id=id)
            self.branch_angles[(branch['from'], branch['to'])] = 0

    def get_bus_coordinates(self):
        """
        Computes the x and y coordinates for each bus given by the length and angle of each branch.

        Return:
            dict mapping each bus id to a tuple of x and y coordinates

        """
        positions = {self.origin_bus: (0, 0)}
        for edge in nx.edge_dfs(self.graph, self.origin_bus):
            local_r = self.graph.edges[edge]['length']
            local_phi = self.branch_angles[edge]
            positions[edge[1]] = cartesian_sum(positions[edge[0]], polar_to_cart(local_r, local_phi))
        return positions

    def randomize_placement(self, seed=None):
        """
        Assigns random angles to each branch. If seed is given, it will be used so the placement is
        deterministic given the the same seed. If seed is None, the random seed will be picked at random.
        This allows quickly checking different random placements.

        Return:
            The seed is returned so a random placement can be recreated.
        """
        if seed == None:
            seed = random.randint(0,2**32)
        random_generator = random.Random(seed)
        for bus in self.branch_angles.keys():
            self.branch_angles[bus] = random_generator.uniform(0, 360)
        return seed

def draw_grid(grid, demand='apparent'):
    """
    Draw a Grid in a matplotlib plot.

    If a valid demand argument is given, nodes are colored after their demand and
    nodes with no demand are not drawn.

    Arguments:
    grid: Grid
        The Grid to be drawn
    demand: str
        A string specifying which demand type to visualize. Can be either
        'active', 'reactive' or 'apparent'.
    """
    fig, ax = plt.subplots()
    cmap = mpl.cm.autumn
    vmin = 0#math.inf
    vmax = 0
    node_colors = []
    node_sizes = []
    # for node in grid.graph.nodes.values():
    #     if demand == 'active':
    #         node_demand = node['Pd']
    #     elif demand == 'reactive':
    #         node_demand = node['Qd']
    #     elif demand == 'apparent':
    #         node_demand = math.sqrt(node['Qd']**2 + node['Pd']**2)
    #     else:
    #         node_demand = 0
    #     node_colors.append(node_demand)
    #     if node_demand == 0:
    #         node_sizes.append(0)
    #     else:
    #         node_sizes.append(200)
    #     if node_demand < vmin:
    #         vmin = node_demand
    #     if node_demand > vmax:
    #         vmax = node_demand
    # norm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)
    # sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
    # plt.colorbar(sm, label="Demand, " + demand, orientation="horizontal", shrink=.75)
    # nx.draw(grid.graph, pos=grid.get_bus_coordinates(), ax=ax, with_labels=True, node_color=node_colors, node_size=node_sizes, cmap=cmap)
    nx.draw(grid.graph, pos=grid.get_bus_coordinates(), ax=ax, with_labels=True, cmap=cmap)
    plt.axis('on')
    ax.tick_params(left=True, bottom=True, labelleft=True, labelbottom=True)
    return ax

def draw_base_stations(base_stations):
    """
    Add base stations to a matplotlib plot. They will show ut as red dots labeled by their id.

    Arguments:
    base_stations: Grid
        A dict mapping a base station id string to a tuple of floats giving the x and y coordinate
        of the base station.

    Example:
        base_stations = {
            'bs1': (0, 1),
            'bs2': (5, -2)
        }
    """
    x = []
    y = []
    ids = []
    for id, coord in base_stations.items():
        x.append(coord[0])
        y.append(coord[1])
        ids.append(id)
    plt.scatter(x, y, color='red')
    for i, txt in enumerate(ids):
        plt.annotate(txt, (x[i], y[i]))

def euclidian_distance(coord1, coord2):
    """Computes distance between two cartesian coordinates"""
    return math.sqrt((coord1[0]-coord2[0])**2 + (coord1[1]-coord2[1])**2)

def base_station_distances(grid, base_stations):
    """
    Computes the distance from each bus in the grid to each base_station.

    Returns:
        A dict of dicts mapping each pair of bus and base station to a distance.
        Example for example dist['bus1']['bs2'] gives the distance from the bus 'bus1' to the
        base station 'bs2'.
    """
    distances = {bus: {} for bus in grid.graph.nodes}
    bus_coordinates = grid.get_bus_coordinates()
    for bus in grid.graph.nodes:
        for bs in base_stations.keys():
            distances[bus][bs] = euclidian_distance(bus_coordinates[bus], base_stations[bs])
    return distances
