# powergridviz

This is a tool for visualizing a power grid given by a set of buses, branches
and their lengths.

## Installation
1. Clone the repo
    ```bash
    $ git clone <repo>
    $ cd powergridviz
    ```
1. (Optional) Set up a vritual environment. There are several ways of doing
this. Here is a simple way to create a virtal environment named `env` in the
project folder.
    ```bash
    $ python3 -m venv env
    ```
    To enable it, run
    ```bash
    $ source env/bin/activate
    ```
1. Install dependencies
    ```bash
    $ pip install -r requirements.txt
    ```
1. (Optional) Run tests
    ```bash
    $ pytest
    ```
1. Check the demo notebook for examples of how to use the tool.
    ```bash
    $ jupyter notebook
    ```
    and open `demo.ipynb`.

## Project structure
The underlying functionality is implemented in `utils.py`. It contains 
docstrings explaining the use of each function. Using notebooks is recommended
for examining the grids, as shown in the demo notebook.

The grid descriptions are given in YAML-files stored in the `grids` directory.
See the file `grids/test_grid.yaml` for an example of how to specify a grid
based on a line diagram and branch lenghts.
